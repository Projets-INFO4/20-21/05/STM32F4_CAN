# Communication between Keyestudio CAN-BUS Shield MCP2515 and STM32F411RE

## What do you need?
- [STM32F411RE board](https://www.st.com/en/evaluation-tools/nucleo-f411re.html "Product page STM32F411RE")
- [Keyestudio CAN-BUS Shield MCP2515](https://wiki.keyestudio.com/KS0411_keyestudio_CAN-BUS_Shield "Wiki page of the board")
- [Arduino IDE](https://www.arduino.cc/en/software "Arduino IDE webpage")

## How to make it work?
1. Connect the shield and the F411RE board:<p align="center"><img src="docs/cardsConnected.jpg" alt="Shield and F411RE connected"></p>
2. Put the **libraries/** content (folder **mcp2515**) in your library Arduino folder (check which is yours in field "Sketchbook location" in "File" -> "Preferences"):<p align="center"><img src="docs/libraryFolder.jpg" alt="Library folder"></p>
3. Open CAN_read.ino or CAN_send.ino with Arduino IDE
4. Go to "File" -> "Preferences" and add ```https://raw.githubusercontent.com/stm32duino/BoardManagerFiles/master/STM32/package_stm_index.json``` in the field "Additional Boards Manager URLs":<p align="center"><img src="docs/urlBoards.jpg" alt="URL boards Arduino IDE"></p>
5. Go to "Tools" -> "Board" -> "Boards Manager". Search for "STM32" and install the last package:<p align="center"><img src="docs/boardsManager.jpg" alt="Boards manager Arduino IDE"></p>
6. Go to "Tools" and settle your board (Note: in "Port", you should select the COM corresponding of your board):<p align="center"><img src="docs/boardSettings.jpg" alt="Settings to flash the board"></p>
7. Now everything is settle, you can flash the code by clicking the "Upload" button!<p align="center"><img src="docs/uploadButton.jpg" alt="Upload button Arduino IDE"></p>
8. You can connect your Shield to your systems. For us:<p align="center"><img src="docs/wholeSystem.jpg" alt="Shield and STM32F7"></p>

## Example CAN_read.ino
<p align="center"><img src="docs/canRead.jpg" alt="CAN_read.ino"></p>
This program simulate a CAN bus and read CAN messages it receives and print them in the Serial Monitor.
If you do not receive any message, make sure the bitrate is the right one (line 12 ```mcp2515.setBitrate(CAN_1000KBPS);```)  and change it if it is not.

## Example CAN_send.ino
<p align="center"><img src="docs/canSend.jpg" alt="CAN_send.ino"></p>
This program simulate a CAN bus and send random CAN messages
If your systems which read those CAN messages do not receive any message, make sure the bitrate is the same for everyone and is the same than the program one (line 14 ```mcp2515.setBitrate(CAN_1000KBPS);```)  and change it if it is not.


## Note
This code comes from [autowp Github Repository](https://github.com/autowp/arduino-mcp2515 "MCP2515 Repository of autowp") and was modified by *Lucas DREZET* to fit our applications