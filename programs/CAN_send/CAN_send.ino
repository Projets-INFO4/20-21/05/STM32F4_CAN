#include <SPI.h>
#include <mcp2515.h>

struct can_frame canMsg1;
struct can_frame canMsg2;
MCP2515 mcp2515(10);


void setup() { 
  while (!Serial);
  Serial.begin(115200);
  
  mcp2515.reset();
  mcp2515.setBitrate(CAN_1000KBPS);
  mcp2515.setNormalMode();
}

void loop() {
  canMsg1.can_id  = random(0x202, 0x207);
  canMsg1.can_dlc = 1;
  canMsg1.data[0] = random(0x0, 0x100);
  mcp2515.sendMessage(&canMsg1);
  delay(10);

  canMsg2.can_id  = random(0x182, 0x187);
  canMsg2.can_dlc = 2;
  canMsg2.data[0] = random(0x0, 0xff);
  canMsg2.data[1] = random(0x0, 0xf);
  mcp2515.sendMessage(&canMsg2);
  delay(10);
}
